(function() {
	'use strict';

	angular
	.module('application')
	.controller('TestController', TestController);

	/** @ngInject */
	function TestController($scope, $state, MainService) {
		var vm = this,
			currectAnsvers = 0;

    vm.user = MainService.getUser();
		vm.questionsCount = vm.user.qustionsCount;
		vm.currentAnsver = null;
		vm.tests = MainService.getQuestions();
		vm.testNumber = 0;
		vm.currentTest = vm.tests[vm.testNumber];
		vm.checked = true;
		vm.answerField = '';

    $scope.time = 0;

    $scope.$on('timer-stopped', function (){
      $state.go('results');
    });

    refreshTimer();

		if (!vm.tests.length) {
			$state.go('login');
		}

		vm.nextQuestion = function () {
			vm.checked = true;
			if (vm.currentTest.type === 'variant') {
				if (vm.currentTest.ansver == vm.currentAnsver) {
					currectAnsvers++;
					MainService.setCorrectAnsvers(currectAnsvers);
				}
			}else if (vm.currentTest.type === 'open') {
				MainService.setOpenQustionAnsver(vm.currentAnsver);
			}

      refreshTimer();

			// debugger;
			if (vm.testNumber < (vm.tests.length - 1) && (vm.testNumber + 1) < (vm.questionsCount + 5)) {
				vm.testNumber = vm.testNumber + 1;
				vm.currentTest = vm.tests[vm.testNumber];
				vm.answerField = '';
			}else {
				$state.go('results');
				vm.answerField = '';
			}
		};

		vm.setAnsver = function (val) {
			vm.currentAnsver = val;
			vm.checked = false;
		};

    function refreshTimer() {
      if (vm.currentTest.time > 1) {
        $scope.time = parseInt(vm.currentTest.time);
        $scope.$broadcast('timer-set-countdown-seconds', $scope.time);
      }
    }
	}
})();



