(function() {
	'use strict';

	angular
	.module('application')
	.controller('ResultsController', ResultsController);

	/** @ngInject */
	function ResultsController($state, MainService, MainRestService) {
		var vm = this;

		vm.user = MainService.getUser();
		vm.questions = MainService.getQuestions();
		vm.variantQustionsCount = vm.user.variantQustionsCount;

		if (!vm.user.password) {
			$state.go('login'); 
		}else {
			MainRestService.setResult(vm.user);
		}

		vm.restartTest = function() {
			$state.go('login');
		};
	}
})();



