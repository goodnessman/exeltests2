(function () {
'use strict';

angular.module('application')
.factory('LoginService', function () {

    var Service = {

        // Returns arr with objects with tests
        parseTests: function (arr) {
            var arr = arr.split('<br />'),
                result = [],
                obj = {},
                property = true,
                propName = null,
                variantsCount = 0;

            if (!arr) return true;

            angular.forEach(arr, function(value, key) {
                if (value === '') return;
                if (value === 'number' && key != 0) {
                    result.push(obj);
                    obj = {};
                }
                if (!variantsCount) {
                    if (property) {
                        obj[value] = null;
                        propName = value;
                        property = !property;
                    }else {
                        obj[propName] = value;
                        property = !property;
                        if (propName === 'variantsCount') {
                            variantsCount = obj.variantsCount;
                            obj.variantsCount;
                        }
                    }
                }else {
                    if (value != 'variants') {
                        obj.variants.push(value);
                        obj.variants.sort(compareRandom);
                        variantsCount = variantsCount - 1;

                    }else {
                        obj.variants = [];
                    }
                }
            });
            result.push(obj);

            function compareRandom(a, b) {
              return Math.random() - 0.5;
            }

            result.sort(compareRandom);

            return result;
        },

        parseOpenQuestions: function (arr) {
            var arr = arr.split('<br />'),
                result = [],
                obj = {},
                property = true,
                propName = null;

            angular.forEach(arr, function(value, key) {
                if (value === '') return;
                if (value === 'number' && key != 0) {
                    result.push(obj);
                    obj = {};
                }
                if (property) {
                    obj[value] = null;
                    propName = value;
                    property = !property;
                }else {
                    obj[propName] = value;
                    property = !property;
                }
            });
            result.push(obj);

            return result;
        },

        addOpenQuestions: function(openQuestions, questions) {
            questions = questions ? questions : [];
            angular.forEach(openQuestions, function(value, key) {
                questions.push(value);
            });

            return questions;
        },

        limiteQuestions: function(arr, num) {
            var result = [];

            for (var i = 0; i < num; i++) {
                result.push(arr[i]);
            }
            return result;
        }
    };

    return Service;
});

})();
