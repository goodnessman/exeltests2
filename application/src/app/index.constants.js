/* global malarkey:false, moment:false */
(function() {
	'use strict';

	angular
	.module('application')
	.constant('malarkey', malarkey)
	.constant('moment', moment)
	.constant('config', {
		api: {
			questions: 'http://localhost.exeltests2/getQuestions.php',
			openQuestions: 'http://localhost.exeltests2/getOpenQuestions.php',
			setResult: 'http://localhost.exeltests2/addResultToExcel.php',
			password: 'http://localhost.exeltests2/getPassword.php',
			questionsCount: 'http://localhost.exeltests2/getQuestionsCount.php'
		}
	});

})();
