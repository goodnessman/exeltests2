
(function () {
'use strict';

angular.module('application')
.factory('MainService', function () {
    var questions = [],
        user = {};

        user.name = '';
        user.course = '';
        user.password = '';
        user.correctAnsvers = 0;
        user.qustionsCount = 0;
        user.variantQustionsCount = 0;
        user.openQuestionsAnsvers = [];

    var Service = {

        setQuestions: function(arr) {
            questions = arr;
        },

        getQuestions: function() {
            return questions;
        },

        setUser: function(obj) {
            user = obj;
        },

        getUser: function() {
            return user;
        },

        setCorrectAnsvers: function(str) {
            user.correctAnsvers = str;
        },

        setQustionsCount: function(data) {
            user.qustionsCount = data;
            console.log(user);
        },

        setOpenQuestions: function(arr) {
            user.openQuestions = arr;
        },

        getOpenQuestions: function() {
            return user.openQuestions;
        },

        setOpenQustionAnsver: function(data) {
            user.openQuestionsAnsvers.push(data);
        },

        setVariantQustionsCount: function(num) {
            user.variantQustionsCount = num;
        },

        getVariantQustionsCount: function(num) {
            return user.variantQustionsCount;
        }
    };

    return Service;
});

})();