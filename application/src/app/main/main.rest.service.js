(function () {
'use strict';

angular.module('application')
.factory('MainRestService', function ($http, $q, config) {
    var Service = {

        getTests: function () {
            var deferred = $q.defer();

            $http.get(config.api.questions).success(function (response, status, headers, config) {

                deferred.resolve(response, status, headers, config);
            }).error(function (response, status, headers, config) {
                
                deferred.reject(response, status, headers, config);
            });

            return deferred.promise;
        },

        getOpenQuestions: function () {
            var deferred = $q.defer();

            $http.get(config.api.openQuestions).success(function (response, status, headers, config) {

                deferred.resolve(response, status, headers, config);
            }).error(function (response, status, headers, config) {
                
                deferred.reject(response, status, headers, config);
            });

            return deferred.promise;
        },

        setResult: function (data) {
            $.ajax({
                type: "POST",
                url: config.api.setResult,
                data: {'myData':data},
                error: function(e){
                    console.log(e.message);
                }
            });
        },

        getPassword: function() {
            var deferred = $q.defer();

            $http.get(config.api.password).success(function (response, status, headers, config) {

                deferred.resolve(response, status, headers, config);
            }).error(function (response, status, headers, config) {
                
                deferred.reject(response, status, headers, config);
            });

            return deferred.promise;
        },

        getQuestionsCount: function() {
            var deferred = $q.defer();

            $http.get(config.api.questionsCount).success(function (response, status, headers, config) {

                deferred.resolve(response, status, headers, config);
            }).error(function (response, status, headers, config) {
                
                deferred.reject(response, status, headers, config);
            });

            return deferred.promise;
        }
    };

    return Service;
});

})();