<?php 

date_default_timezone_set('America/Los_Angeles');

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT');

/** Include PHPExcel */
require_once 'PHPExcel/Classes/PHPExcel.php';
require_once 'PHPExcel/Classes/PHPExcel/IOFactory.php';

$obj = $_POST['myData'];

$objPHPExcel = PHPExcel_IOFactory::load("results.xlsx");
$objPHPExcel->setActiveSheetIndex(0);
$row = $objPHPExcel->getActiveSheet()->getHighestRow()+1;
//echo $row;
$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $obj['name']);
$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $obj['vk']);
$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $obj['email']);
$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $obj['correctAnsvers']);
$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $obj['tel']);
$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $obj['course']);
$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $obj['openQuestionsAnsvers'][0]);
$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $obj['openQuestionsAnsvers'][1]);
$objPHPExcel->getActiveSheet()->SetCellValue('I'.$row, $obj['openQuestionsAnsvers'][2]);
$objPHPExcel->getActiveSheet()->SetCellValue('J'.$row, $obj['openQuestionsAnsvers'][3]);
$objPHPExcel->getActiveSheet()->SetCellValue('K'.$row, $obj['openQuestionsAnsvers'][4]);
$objPHPExcel->getActiveSheet()->SetCellValue('L'.$row, $obj['openQuestionsAnsvers'][5]);
$objPHPExcel->getActiveSheet()->SetCellValue('M'.$row, $obj['openQuestionsAnsvers'][6]);
$objPHPExcel->getActiveSheet()->SetCellValue('N'.$row, $obj['openQuestionsAnsvers'][7]);
$objPHPExcel->getActiveSheet()->SetCellValue('O'.$row, $obj['openQuestionsAnsvers'][8]);
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
$objWriter->save('results.xlsx');


